# Usage

## generate url

```bash
npm install
```

then

```bash
node ./tsk.js
```

or

```bash
./tsk.sh
```

## use with the bundled js

```bash
npm run doWebpack
```

then

```bash
node ./dist/common.js
```
