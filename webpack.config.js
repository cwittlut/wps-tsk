const path = require('path');

module.exports = {
  // mode: 'development',
  mode: 'production',
  entry: {
    common: './tsk.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
};
