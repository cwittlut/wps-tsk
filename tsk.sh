#!/usr/bin/env bash
#

url='https://wps-linux-personal.wpscdn.cn/wps/download/ep/Linux2019/11711/wps-office_11.1.0.11711_amd64.deb'
uri="${url#https://wps-linux-personal.wpscdn.cn}"
secrityKey='7f8faaaa468174dc1c9cd62e5f218a5b'
timestamp10=$(date '+%s')
md5hash=$(echo -n "${secrityKey}${uri}${timestamp10}" | md5sum)
url+="?t=${timestamp10}&k=${md5hash%% *}"
echo "$url"
